package lista;
import java.util.NoSuchElementException;

/**
* Manejo de Datos 2019-1
* Prof. Adrian Ulises Mercado Martinez
* Ayudante: Diego Alberto Magallanes Ramirez
*
* Practica 1: Aseret Guzman Luna
* Clase que representa una lista doblemente ligada.
*/
public class Lista {

	/* Clase Nodo privada para uso interno de la clase Lista. */
	private class Nodo {
		/* El elemento del nodo. */
		public int elemento;
		/* El nodo anterior. */
		public Nodo anterior;
		/* El nodo siguiente. */
		public Nodo siguiente;

		/* Construye un nodo con el elemento especificado. */
		public Nodo(int elemento) {
			this.elemento = elemento;
		}
	} // Fin de la clase Nodo.

	/* Primer elemento de la lista. */
	private Nodo cabeza;
	/* Ultimo elemento de la lista. */
	private Nodo rabo;
	/* Numero de elementos en la lista. */
	private int longitud;

	/**
	 * Regresa la longitud de la lista
	 * @return la longitud de la lista, el numero de elementos que contiene.
	*/
	public int getLongitud() {
		return longitud;
	}

	/**
	 * Metodo que nos indica si la una lista esta vacia.
	 * @return <code>true</code> si la cabeza es nula, <code>false</code> e.o.c.
	*/
	public boolean esVacia() {
            return cabeza == null;
	}

	/**
	 * Regresa el primer elemento de la lista.
	 * @return El primer elemento de la lista.
	 * @throws NoSuchElementException si la lista es vacia.
	*/
	public int getPrimero() {
            if(esVacia())
                throw new NoSuchElementException("La lista es vacia");
            
            return cabeza.elemento;
	}

	/**
	 * Regresa el ultimo elemento de la lista.
	 * @return El ultimo elemento de la lista.
	 * @throws NoSuchElementException si la lista es vacia.
	*/
	public int getUltimo() {
            if(esVacia())
                throw new NoSuchElementException("La lista es vacia");
            
            return rabo.elemento;
	}

	/**
	 * Agrega un elemento al final de la lista. Si la lista no tiene elementos,
	 * entonces el elemento a agregar sera el primero y el ultimo a la vez.
	 * @param elemento El elemento a agregar.
	*/
	public void agregaFinal(int elemento) {
            Nodo nuevo = new Nodo(elemento);
		if(esVacia()){
                    this.cabeza = nuevo;
                    this.rabo = nuevo;
                    longitud ++;
                }else{
                    rabo.siguiente = nuevo;
                    nuevo.anterior = rabo;
                    rabo = nuevo;
                    longitud ++;
                }
	}

	/**
	 * Agrega un elemento al inicio de la lista. Si la lista no tiene elementos,
	 * entonces el elemento a agregar sera el primero y el ultimo a la vez.
	 * @param elemento El elemento a agregar
	*/
	public void agregaInicio(int elemento) {
		Nodo nuevo = new Nodo (elemento);
                if(esVacia()){
                    this.cabeza = nuevo;
                    this.rabo = nuevo;
                    longitud ++;
                }else{
                    cabeza.anterior = nuevo;
                    nuevo.siguiente = cabeza;
                    cabeza = nuevo;
                    longitud ++;
                }
	}

	/**
	 * Elimina el primer elemento de la lista y lo regresa.
	 * @return El primer elemento de la lista antes de ser eliminado.
	 * @throws NoSuchElementException si la lista es vacia.
	*/
	public int eliminaPrimero() {
            if(esVacia())
                throw new NoSuchElementException("La lista es vacia");
            int e = getPrimero();
		cabeza = cabeza.siguiente;
                        longitud --;
		return e;
	}

	/**
	 * Elimina el ultimo elemento de la lista y lo regresa.
	 * @return El ultimo elemento de la lista antes de ser eliminado.
	 * @throws NoSuchElementException si la lista es vacia.
	*/
	public int eliminaUltimo() {
            if(esVacia())
                throw new NoSuchElementException("La lista es vacia");
            int e = getUltimo();
                Nodo temp = rabo.anterior;
                rabo.anterior.siguiente = null;
                rabo = temp;
                longitud --;
		return e;
	}

	/**
	 * Elimina un elemento de la lista. Si el elemento no esta contenido en la
	 * lista, el metodo no hace nada. Si el elemento aparece varias veces en la
	 * lista, el metodo elimina la primera aparicion.
	 * @param elemento El elemento a eliminar.
	*/
	public void elimina(int elemento) {
		Nodo n = buscaNodo(cabeza, elemento);
                if(n==null)
                    return;
                else{
                    if(n.elemento == cabeza.elemento)
                        eliminaPrimero();
                        
                    else{
                        if(n.elemento == rabo.elemento)
                            eliminaUltimo();
                        else{
                            n.anterior.siguiente = n.siguiente;
                            n.anterior.anterior = n.anterior;
                        }
                    }
                    longitud --;
                }
                
	}

	/* Metodo auxiliar para usarlo en el metodo contiene. */
	private Nodo buscaNodo(Nodo n, int elemento) {
		if(n==null)
		    return null;
                if(n.elemento == elemento)
                    return n;
                return buscaNodo(n.siguiente, elemento);
	}

	/**
	 * Nos dice si un elemento esta en la lista.
	 * @param elemento El elemento que queremos saber si esta en la lista.
	 * @return <tt>true</tt> si <tt>elemento</tt> esta en la lista, <tt>false</tt> e.o.c.
	*/
	public boolean contiene(int elemento) {
		Nodo n = cabeza;
                if(buscaNodo(n,elemento)== null)
                    return false;
                else
                    return true;
	}

	/**
	 * Regresa la reversa de la lista.
	 * @return Una nueva lista que es la reversa de la que manda a llamar al metodo.
	*/
	public Lista reversa() {
            Lista reversa = new Lista();
                for(Nodo temp = cabeza; temp!= null; temp = temp.siguiente){
                    reversa.agregaInicio(temp.elemento);
                }
                return reversa;
	}

	/**
	 * Regresa una copia de la lista. La copia tiene los mismos elementos que la
	 * lista que manda a llamar el metodo, en el mismo orden.
	 * @return Una copia de la lista.
	*/
	public Lista copia() {
		Lista copia = new Lista();
                for(Nodo temp = cabeza; temp!= null; temp = temp.siguiente){
                    copia.agregaFinal(temp.elemento);
                }
                
		return copia;
	}

	/**
	 * Limpia la lista de elementos. El llamar este metodo es equivalente a eliminar
	 * todos los elementos de la lista.
	*/
	public void limpia() {
		this.cabeza = null;
                this.longitud = 0;
	}

	/**
	 * Regresa el i-esimo elemento de la lista.
	 * @param i El indice del elemento que queremos.
	 * @return El i-esimo elemento de la lista, si es mayor o igual a 0 y menor que 
	 * el numero de elementos en la lista.
	 * @throws ExcepcionIndiceInvalido si el indice recibido es menos que cero, o 
	 * mayor que el numero de elemntos en la lista menos uno.
	*/	
	public int get(int i) {
            int j;
            Nodo temp = cabeza;
		if(i<0 || i>longitud)
                    throw new ExcepcionIndiceInvalido("El valor del indice no es valido!");
                else{
                    if(i==0){
                        return temp.elemento;
                    }else{
                        for(j=0;j<i;j++){
                            temp = temp.siguiente;
                        }
                    }
                    
                }
		return temp.elemento;
	}

	/**
	 * Regresa el indice del elemento recibido.
	 * @param elemento El elemento del que se busca el indice.
	 * @return El indice del elemento recibido, o -1 si el elemento no esta contenido en la lista.
	*/
	public int getIndice(int elemento) {
                    int i=0;
                    if(contiene(elemento) == false)
                        return -1;
                    else{
                        for(Nodo temp = cabeza; temp!= null; temp = temp.siguiente)
                        {
                            if(temp.elemento == elemento){
                                return i;
                            }
                            else{
                                i++;
                            }
                        }
                    }
		return i+1;
	}

	/* Metodo auxiliar para getIndice. */
	private int indice(int elemento, Nodo nodo, int i) {
		return 0;
	}

	/**
	 * Nos dice si la lista es igual al objeto recibido.
	 * @param o El objeto con el que hay que comparar.
	 * @return <tt>true</tt> si la lista es igual al objeto recibido;
	 * <tt>false</tt> e.o.c.
	*/
	@Override public boolean equals(Object o) {
		if(o == null)
			return false;
		if(getClass() != o.getClass())
			return false;
		@SuppressWarnings("unchecked") Lista nueva = getClass().cast(o);
		if(longitud != nueva.longitud)
			return false;
		Nodo nodo1 = cabeza;
		Nodo nodo2 = nueva.cabeza;
		while(nodo1 != null) {
			if(!(nodo1.elemento == nodo2.elemento))
				return false;
			nodo1 = nodo1.siguiente;
			nodo2 = nodo2.siguiente;
		}
		return true;
	}

	/**
	 * Regresa una representacion en cadena de la lista.
	 * @return Una representacion en cadena de la lista.
	*/
	@Override public String toString() {
		String s = "[";
		if(longitud == 0)
			return s += "]";
		Nodo auxiliar = cabeza;
		while(auxiliar.siguiente != null) {
			s += String.format("%S, ", auxiliar.elemento);
			auxiliar = auxiliar.siguiente;
		}
		s += String.format("%S]", rabo.elemento);
		return s;
	}
}